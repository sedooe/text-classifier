#!/bin/bash

echo "YES" | pio app data-delete NaiveBayes
cp /home/sedat/Desktop/code/PatternRecognitionProject/data/output.json /home/sedat/Desktop/code/NaiveBayes/data/
cd /home/sedat/Desktop/code/NaiveBayes
pio import --appid 4 --input data/stopwords.json
pio import --appid 4 --input data/output.json
pio build
pio train
pio deploy&
gnome-terminal --tab -e "./getresult.sh"

