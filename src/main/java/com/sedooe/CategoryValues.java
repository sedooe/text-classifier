package com.sedooe;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class CategoryValues {
    @Id
    @Column(unique = true)
    @NotNull
    private Category category;

    @Column(unique = true)
    @NotEmpty
    private String categoryName;

    @OneToMany(mappedBy = "category")
    private Set<NewsSource> newsSourceList;

    public CategoryValues() {
    }

    public CategoryValues(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getCategoryLabel() {
        return this.category.label;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<NewsSource> getNewsSourceList() {
        return newsSourceList;
    }

    public void setNewsSourceList(Set<NewsSource> newsSourceList) {
        this.newsSourceList = newsSourceList;
    }

    public enum Category {
        politics(100),
        technology(200),
        science(250),
        business(50),
        economics(40),
        sport(500)
        ;

        private double label;

        public double getLabel() {
            return this.label;
        }

        Category(double label) {
            this.label = label;
        }
    }
}