package com.sedooe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ExportDB {
    private static final AtomicInteger id = new AtomicInteger(0);

    public static void main(String[] args) throws IOException {

//        List<NewsSource> queryResult = session.createCriteria(NewsSource.class)
//                .add(Restrictions.eq("category", CategoryValues.Category.valueOf("politics")))
//                .add(Restrictions.sqlRestriction("1=1 order by random()"))
//                .setMaxResults(200)
//                .list();

        List<NewsSource> data = new ArrayList<>();

        for (CategoryValues.Category category: CategoryValues.Category.values()) {
            data.addAll(getRandomData(category, 2000));
        }

        StringBuilder jsonInString = prepareData(data);

        writeToFile("/home/sedat/Desktop/code/PatternRecognitionProject/data/output.json", jsonInString);

        System.out.println(data.size());

        System.exit(0);
    }

    public static List<NewsSource> getRandomData(CategoryValues.Category category, int amount) {
        SessionFactory sessionFactory = new Configuration().configure()
                .buildSessionFactory();
        Session session = sessionFactory.openSession();

        CategoryValues queryResult = (CategoryValues) session.createCriteria(CategoryValues.class)
                .add(Restrictions.eq("category", category))
                .uniqueResult();

        List<NewsSource> news = new ArrayList<>();
        news.addAll(queryResult.getNewsSourceList());
        Collections.shuffle(news); //randomize

        List<NewsSource> randomData = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            randomData.add(news.get(i));
        }

        return randomData;
    }

    public static StringBuilder prepareData(Iterable<NewsSource> queryResult) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        StringBuilder jsonInString = new StringBuilder();

        for (NewsSource newsSource: queryResult) {
            int currentId = id.addAndGet(1);
            TrainingData trainingData = new TrainingData(currentId, newsSource.getCategoryValues().getCategory().toString(),
                    newsSource.getText(), newsSource.getCategoryValues().getCategory().getLabel());
            try {
                jsonInString.append(mapper.writeValueAsString(trainingData) + "\n");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        return jsonInString;
    }

    public static void writeToFile(String filePath, StringBuilder jsonInString) {
        Path path = Paths.get(filePath);

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write(jsonInString.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
