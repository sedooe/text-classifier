package com.sedooe;

interface Data {
    long getId();

    String getRssUrl();

    void setRssUrl(String rssUrl);

    void setCategoryValues(CategoryValues category);

    void setCssSelector(String cssSelector);

    void setSource(NewsSource.Source source);

    CategoryValues getCategoryValues();

    String getCssSelector();

    NewsSource.Source getSource();

    String getNewsUrl();

    void setNewsUrl(String newsUrl);

    String getText();

    void setText(String text);
}
