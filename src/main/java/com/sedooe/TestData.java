package com.sedooe;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class TestData implements Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Transient
    private String rssUrl;

    @Column(unique = true, columnDefinition = "TEXT")
    @NotEmpty
    private String newsUrl;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "category")
    private CategoryValues category;

    @Transient
    private NewsSource.Source source;

    @NotEmpty
    private String cssSelector;

    @Column(columnDefinition = "TEXT")
    @NotEmpty
    private String text;

    public TestData() {
    }

    public TestData(String newsUrl, CategoryValues.Category category, String cssSelector, String text) {
        this.newsUrl = newsUrl;
        this.category = new CategoryValues(category);
        this.cssSelector = cssSelector;
        this.text = text;
    }

    public TestData(String rssUrl, CategoryValues.Category category, String cssSelector) {
        this.rssUrl = rssUrl;
        this.category = new CategoryValues(category);
        this.cssSelector = cssSelector;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getRssUrl() {
        return rssUrl;
    }

    public void setRssUrl(String rssUrl) {
        this.rssUrl = rssUrl;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    @Override
    public CategoryValues getCategoryValues() {
        return category;
    }

    @Override
    public void setCategoryValues(CategoryValues category) {
        this.category = category;
    }

    public String getCssSelector() {
        return cssSelector;
    }

    public void setCssSelector(String cssSelector) {
        this.cssSelector = cssSelector;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public NewsSource.Source getSource() {
        return source;
    }

    @Override
    public void setSource(NewsSource.Source source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "TestData{" +
                "id='" + id + '\'' +
                ", rssUrl='" + rssUrl + '\'' +
                ", newsUrl='" + newsUrl + '\'' +
                ", category=" + category +
                ", cssSelector='" + cssSelector + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
