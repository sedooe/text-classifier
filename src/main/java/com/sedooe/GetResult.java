package com.sedooe;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import io.prediction.EngineClient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class GetResult {
    private static Map<String, Integer> successPredictionMap = new HashMap<>();

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        List<TestData> results = session.createCriteria(TestData.class).list();

        for (TestData data: results) {
            String text = data.getText();
            String category = data.getCategoryValues().getCategory().toString();

            // assuming science and tech represent the same category.
//            if ((category.equals("science") && getResult(text).equals("technology"))
//                    || (category.equals("technology") && getResult(text).equals("science"))) {
//                updateCounter(category);
//                continue;
//            }

            // assuming business and economics represent the same category.
//            if ((category.equals("business") && getResult(text).equals("economics"))
//                    || (category.equals("economics") && getResult(text).equals("business"))) {
//                updateCounter(category);
//                continue;
//            }

            if (!category.equals(getResult(text))) {
                System.out.println(category + " - " + getResult(text));
                System.out.println(data.getCssSelector());
                System.out.println(data.getNewsUrl() + "\n");
            } else {
                updateCounter(category);
            }
        }

        int successfulPrediction = 0;
        for (int i: successPredictionMap.values()) {
            successfulPrediction += i;
        }

        double successRate = successRate(successfulPrediction, results.size());

        printCategoryStats(session);
        System.out.println("Total prediction: " + results.size());
        System.out.println("Successful prediction: " + successfulPrediction);
        System.out.println("Success rate: %" + successRate);

        FileWriter fileWritter = new FileWriter(new File("/home/sedat/Desktop/results.txt"), true);
        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
        bufferWritter.write("%" + successRate + "\n");
        bufferWritter.close();

        session.close();

        System.exit(0);
    }

    public static String getResult(String text) throws IOException, ExecutionException, InterruptedException {
        EngineClient engineClient = new EngineClient("https://localhost:8000");

        JsonObject response = engineClient.sendQuery(ImmutableMap.<String, Object>of(
                "text",
                text
        ));

        return response.get("category").getAsString();
    }

    public static double successRate(int sucessfulPrediction, int totalPrediction) {
        return (double) (sucessfulPrediction * 100) / totalPrediction;
    }

    public static void updateCounter(String category) {
        if (!successPredictionMap.containsKey(category))
            successPredictionMap.put(category, 1);
        else
            successPredictionMap.put(category, successPredictionMap.get(category)+1);
    }

    public static void printCategoryStats(Session session) {
        ProjectionList projectionList = Projections.projectionList()
                .add(Projections.groupProperty("category"))
                .add(Projections.rowCount());

        List<Object[]> resultSet = session.createCriteria(TestData.class).setProjection(projectionList).list();

        Map<String, Long> map = new HashMap<>();
        for (Object[] obj: resultSet) {
            CategoryValues category = (CategoryValues) obj[0];
            map.put(category.getCategory().toString(), (Long) obj[1]);
        }

        for (Map.Entry<String, Integer> entry: successPredictionMap.entrySet()) {
            String category = entry.getKey();
            Integer correctPredictions = entry.getValue();
            System.out.println(category + " " + correctPredictions + "/" + map.get(category));
        }
    }
}