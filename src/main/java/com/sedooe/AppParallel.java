/*
 * Copyright (C) 11, 2015 Kod Gemisi Ltd. <foss@kodgemisi.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sedooe;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


public class AppParallel {
    private final static boolean debug = false;

    private static final AtomicInteger id = new AtomicInteger(0);
    private static final int TIMEOUT = 20000;
    private static final int PARALLELISM = 5;

    public static void main(String[ ] args) throws ExecutionException, InterruptedException {
        Collection<Data> testData = Arrays.asList(
//                new TestData("http://talksport.com/rss/sports-news/rugby-league/feed", CategoryValues.Category.sport, ".field-item.even"),
//                new TestData("http://talksport.com/rss/sports-news/football/feed", CategoryValues.Category.sport, ".field-item.even"),
//                new TestData("http://talksport.com/rss/sports-news/golf/feed", CategoryValues.Category.sport, ".field-item.even"),

//                new TestData("http://www.infoworld.com/news/index.rss", CategoryValues.Category.technology, "#drr-container"),
//                new TestData("http://www.infoworld.com/blog/all/index.rss", CategoryValues.Category.technology, "#drr-container"),
//                new TestData("http://zdnet.com.feedsportal.com/c/35462/f/675634/index.rss", CategoryValues.Category.technology, ".storyBody"),
//                new TestData("http://zdnet.com.feedsportal.com/c/35462/f/675633/index.rss", CategoryValues.Category.technology, ".storyBody"),

//                new TestData("http://www.firstpost.com/category/politics/feed", CategoryValues.Category.politics, ".fullCont1"),
//                new TestData("http://globalnews.ca/politics/feed/", CategoryValues.Category.politics, "[itemprop=articleBody]"),
//                new TestData("http://www.cbc.ca/cmlink/1.310", CategoryValues.Category.politics, ".story-content"),
//                new TestData("http://www.ctvnews.ca/rss/politics/ctvnews-ca-politics-public-rss-1.822302", CategoryValues.Category.politics, ".articleBody"),

//                new TestData("http://www.theguardian.com/science/rss", CategoryValues.Category.science, "[itemprop=articleBody]"),
//                new TestData("http://feeds.huffingtonpost.com/c/35496/f/677540/index.rss", CategoryValues.Category.science, ".entry-text"),
//                new TestData("http://feeds.esciencenews.com/eScienceNews/popular", CategoryValues.Category.science, ".span-14.append-1"),

//                new TestData("http://feeds.feedburner.com/euronews/en/business/", CategoryValues.Category.business, "#articleTranscript"),
//                new TestData("http://www.theguardian.com/uk/business/rss", CategoryValues.Category.business, "#articleBody"),
//                new TestData("http://www.cbc.ca/cmlink/1.224", CategoryValues.Category.business, ".story-content"),

//                new TestData("http://articlefeeds.nasdaq.com/nasdaq/categories?category=Economy&format=xml", NewsSource.Category.economics, "#articleText")
        );

        Collection<Data> trainData = Arrays.asList(
                new NewsSource("http://www.theguardian.com/us-news/us-politics/rss", CategoryValues.Category.politics, ".content__article-body.from-content-api.js-article__body",
                        NewsSource.Source.theguardian),
                new NewsSource("http://feeds.bbci.co.uk/news/politics/rss.xml", CategoryValues.Category.politics, ".story-body__inner", NewsSource.Source.bbc),
                new NewsSource("http://mf.feeds.reuters.com/reuters/UKWorldNews", CategoryValues.Category.politics, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://www.conflict-news.com/feed/", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=2", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=3", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=4", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=5", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=6", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=7", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=8", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=9", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("http://www.conflict-news.com/feed/?paged=10", CategoryValues.Category.politics, ".post_inner_wrapper .post_inner_wrapper p", NewsSource.Source.conflictnews),
                new NewsSource("https://www.rt.com/rss/politics/", CategoryValues.Category.politics, ".article .article__text.text", NewsSource.Source.rt),

                new NewsSource("http://www.theguardian.com/uk/sport/rss", CategoryValues.Category.sport, ".content__article-body.from-content-api.js-article__body", NewsSource.Source.theguardian),
                new NewsSource("http://www.bbc.com/sport/rss.xml", CategoryValues.Category.sport, ".story-body .article", NewsSource.Source.bbc),
                new NewsSource("http://mf.feeds.reuters.com/reuters/UKSportsNews", CategoryValues.Category.sport, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://sports.yahoo.com/mlb/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),
                new NewsSource("http://sports.yahoo.com/nfl/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),
                new NewsSource("http://sports.yahoo.com/nba/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),
                new NewsSource("http://sports.yahoo.com/golf/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),
                new NewsSource("http://sports.yahoo.com/oly/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),
                new NewsSource("http://sports.yahoo.com/box/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),
                new NewsSource("http://sports.yahoo.com/wnba/rss.xml", CategoryValues.Category.sport, ".body.yom-art-content.clearfix", NewsSource.Source.yahoo),

                new NewsSource("http://www.theguardian.com/business/economics/rss", CategoryValues.Category.economics, ".content__article-body.from-content-api.js-article__body",
                        NewsSource.Source.theguardian),
                new NewsSource("http://mf.feeds.reuters.com/reuters/UKBankingFinancial", CategoryValues.Category.economics, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://feeds.reuters.com/news/economy.xml", CategoryValues.Category.economics, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://feeds.reuters.com/reuters/financialsNews.xml", CategoryValues.Category.economics, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://feeds.reuters.com/UnstructuredFinance.xml", CategoryValues.Category.economics, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://www.cnbc.com/id/10000664/device/rss/rss.html", CategoryValues.Category.economics, "#article_body", NewsSource.Source.cnbc),
                new NewsSource("http://www.telegraph.co.uk/finance/rss", CategoryValues.Category.economics, "#mainBodyArea", NewsSource.Source.telegraph),
                new NewsSource("http://articlefeeds.nasdaq.com/nasdaq/categories?category=Basics&format=xml", CategoryValues.Category.economics, "#articleText", NewsSource.Source.nasdaq),
                new NewsSource("http://articlefeeds.nasdaq.com/nasdaq/categories?category=Banking%20and%20Loans&format=xml", CategoryValues.Category.economics, "#articleText",
                        NewsSource.Source.nasdaq),

                new NewsSource("http://www.theguardian.com/uk/technology/rss", CategoryValues.Category.technology, ".content__article-body.from-content-api.js-article__body",
                        NewsSource.Source.theguardian),
                new NewsSource("http://feeds.bbci.co.uk/news/technology/rss.xml", CategoryValues.Category.technology, ".story-body__inner", NewsSource.Source.bbc),
                new NewsSource("http://mf.feeds.reuters.com/reuters/technologyNews", CategoryValues.Category.technology, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://www.cnbc.com/id/19854910/device/rss/rss.html", CategoryValues.Category.technology, "#article_body", NewsSource.Source.cnbc),
                new NewsSource("http://www.telegraph.co.uk/technology/news/rss", CategoryValues.Category.technology, "#mainBodyArea", NewsSource.Source.telegraph),
                new NewsSource("http://www.computerweekly.com/rss/Internet-technology.xml", CategoryValues.Category.technology, "#content-body", NewsSource.Source.computerweekly),
                new NewsSource("http://www.computerweekly.com/rss/IT-security.xml", CategoryValues.Category.technology, "#content-body", NewsSource.Source.computerweekly),
                new NewsSource("http://www.computerweekly.com/rss/Mobile-technology.xml", CategoryValues.Category.technology, "#content-body", NewsSource.Source.computerweekly),

                new NewsSource("http://feeds.bbci.co.uk/news/business/rss.xml", CategoryValues.Category.business, ".story-body__inner", NewsSource.Source.bbc),
                new NewsSource("https://www.rt.com/rss/business/", CategoryValues.Category.business, ".article .article__text.text", NewsSource.Source.rt),
                new NewsSource("http://feeds.skynews.com/feeds/rss/business.xml", CategoryValues.Category.business, ".content-column", NewsSource.Source.sky),
                new NewsSource("http://rss.cnn.com/rss/money_smbusiness.rss", CategoryValues.Category.business, "#storytext", NewsSource.Source.cnn),
                new NewsSource("http://feeds.foxbusiness.com/foxbusiness/section/news.xml", CategoryValues.Category.business, "[itemprop=\"articleBody\"]", NewsSource.Source.fox),
                new NewsSource("http://feeds.reuters.com/reuters/companyNews.xml", CategoryValues.Category.business, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://www.cnbc.com/id/10001147/device/rss/rss.html", CategoryValues.Category.business, "#article_body", NewsSource.Source.cnbc),
                new NewsSource("http://www.ibtimes.co.uk/rss/companies", CategoryValues.Category.business, "#v_main", NewsSource.Source.ibtimes),
                new NewsSource("http://articlefeeds.nasdaq.com/nasdaq/categories?category=Business&format=xml", CategoryValues.Category.business, "#articleText", NewsSource.Source.nasdaq),

                new NewsSource("http://mf.feeds.reuters.com/reuters/UKScienceNews", CategoryValues.Category.science, "#articleText", NewsSource.Source.reuters),
                new NewsSource("http://feeds.sciencedaily.com/sciencedaily/top_news/top_science.xml", CategoryValues.Category.science, "#text", NewsSource.Source.sciencedaily),
                new NewsSource("http://rss.sciam.com/ScientificAmerican-News.xml", CategoryValues.Category.science, ".article-block.article-text", NewsSource.Source.sciam),
                new NewsSource("http://www.eurekalert.org/rss/chemistry_physics.xml", CategoryValues.Category.science, ".entry", NewsSource.Source.eurekalert),
                new NewsSource("http://www.eurekalert.org/rss/nanotechnology.xml", CategoryValues.Category.science, ".entry", NewsSource.Source.eurekalert),
                new NewsSource("http://www.eurekalert.org/rss/space_planetary.xml", CategoryValues.Category.science, ".entry", NewsSource.Source.eurekalert),
                new NewsSource("http://www.eurekalert.org/rss/atmospheric_science.xml", CategoryValues.Category.science, ".entry", NewsSource.Source.eurekalert),
                new NewsSource("http://www.eurekalert.org/rss/mathematics.xml", CategoryValues.Category.science, ".entry", NewsSource.Source.eurekalert)
        );

        parseNews(trainData);
        //parseNews(testData);
    }

    private static void parseNews(Collection<Data> newsSources) throws ExecutionException, InterruptedException {

        System.out.println("AppParallel.parseNews");

        Map<Long, Session> sessionMap = new ConcurrentHashMap<>();

        ConcurrentLinkedQueue allResults = new ConcurrentLinkedQueue();

        ForkJoinPool forkJoinPool = new ForkJoinPool(PARALLELISM);

        System.out.println("Starting");

        long startTime = System.currentTimeMillis();

        SessionFactory sessionFactory = new Configuration().configure()
                .buildSessionFactory();

        forkJoinPool.submit(() -> newsSources.parallelStream()
                .map(newsSource -> {

                    SyndFeedInput input = new SyndFeedInput();
                    SyndFeed feed = null;
                    try {
                        feed = input.build(new XmlReader(new URL(newsSource.getRssUrl())));
                    } catch (Exception e) {
                        e.printStackTrace();
                        return newsSource;
                    }

                    Long threadId = Thread.currentThread().getId();
                    if(!sessionMap.containsKey(threadId)) {
                        Session session = sessionFactory.openSession();
                        session.setFlushMode(FlushMode.COMMIT);
                        sessionMap.putIfAbsent(threadId, session);
                    }

                    Session session = sessionMap.get(threadId);

//                    System.out.println("1st mapping, thread: " + Thread.currentThread().getId() + " | " + newsSource.getRssUrl());

                    List<Data> result = feed.getEntries().parallelStream().map(syndEntry -> {
                        String newsUrl = syndEntry.getLink();

                        Criteria criteria = session.createCriteria(NewsSource.class);
                        List results = criteria.add(Restrictions.eq("newsUrl", newsUrl)).setMaxResults(1).list();

                        if (!results.isEmpty())
                            return null;

//                        System.out.println("2nd mapping, thread: " + Thread.currentThread().getId() + " | " + newsUrl);

                        Document doc = null;
                        try {
                            doc = Jsoup.connect(newsUrl).timeout(TIMEOUT).get();
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                        Elements newsBody = doc.select(newsSource.getCssSelector());

                        if (debug) {
                            System.out.println("-------------------------------------");
                            System.out.println(newsUrl);
                            System.out.println(newsBody.text());
                        }

                        // skip empty content
                        if (newsBody.text() == null || newsBody.text().trim().length() == 0) {
                            return null;
                        }

                        Data recordedData = null;
                        if (newsSource instanceof NewsSource) {
                            recordedData = new NewsSource(newsUrl, newsSource.getCategoryValues().getCategory(),
                                    newsSource.getCssSelector(), newsBody.text());
                            recordedData.setRssUrl(newsSource.getRssUrl());
                            recordedData.setSource(newsSource.getSource());
                        } else {
                            recordedData = new TestData(newsUrl, newsSource.getCategoryValues().getCategory(), newsSource.getCssSelector(),
                                    newsBody.text());
                        }

                        int currentId = id.addAndGet(1);

                        // DB operations here
                        try {
                            TransactionStatus status = session.getTransaction().getStatus();
                            if (status.isNotOneOf(TransactionStatus.ACTIVE)) {
                                session.getTransaction().begin();
                            }
                            session.persist(recordedData);
                            if (currentId % 20 == 0 && session.getTransaction().getStatus().isNotOneOf(TransactionStatus.COMMITTED, TransactionStatus.COMMITTING)) {
                                session.getTransaction().commit();
                                session.clear();
                            }
                        }
                        catch (Exception e) {
//                            session.getTransaction().rollback();
                            e.printStackTrace();
//                            System.exit(1);
                        }

                        System.out.print(".");
                        return recordedData;
                    })
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList());

                    try {
                        allResults.addAll(result);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return newsSource;
                }).count()).get();


        for(Session s : sessionMap.values()) {
            if (s.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)){
                s.getTransaction().commit();
            }
            s.close();
            System.out.println("closed a session");
        }

        long endTime = System.currentTimeMillis();

        System.out.println("Last id: " + id);
        System.out.println("Duration: " + (endTime - startTime));

        System.exit(0);
    }
}

