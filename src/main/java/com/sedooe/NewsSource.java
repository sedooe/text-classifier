/*
 * Copyright (C) 11, 2015 Kod Gemisi Ltd. <foss@kodgemisi.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sedooe;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class NewsSource implements Data {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    private String rssUrl;

    @Column(unique = true, columnDefinition="TEXT")
    @NotEmpty
    private String newsUrl;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "category")
    private CategoryValues category;

    @NotEmpty
    private String cssSelector;

    @NotNull
    private Source source;

    @Column(columnDefinition="TEXT")
    @NotEmpty
    private String text;

    public NewsSource() {
    }

    public NewsSource(String newsUrl, CategoryValues.Category category, String cssSelector, String text) {
        this.newsUrl = newsUrl;
        this.category = new CategoryValues(category);
        this.cssSelector = cssSelector;
        this.text = text;
    }

    public NewsSource(String rssUrl, CategoryValues.Category category, String cssSelector, Source source) {
        this.rssUrl = rssUrl;
        this.category = new CategoryValues(category);
        this.cssSelector = cssSelector;
        this.source = source;
    }

    public long getId() {
        return id;
    }

    public String getRssUrl() {
        return rssUrl;
    }

    @Override
    public CategoryValues getCategoryValues() {
        return category;
    }

    @Override
    public void setCategoryValues(CategoryValues category) {
        this.category = category;
    }

    public void setRssUrl(String rssUrl) {
        this.rssUrl = rssUrl;
    }

    public void setCssSelector(String cssSelector) {
        this.cssSelector = cssSelector;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getCssSelector() {
        return cssSelector;
    }

    public Source getSource() {
        return this.source;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public enum Source {
        theguardian,
        bbc,
        reuters,
        conflictnews,
        rt,
        yahoo,
        cnbc,
        telegraph,
        computerweekly,
        sky,
        fox,
        ibtimes,
        sciencedaily,
        sciam,
        eurekalert,
        cnn,
        nasdaq
        ;
    }

    @Override
    public String toString() {
        return "NewsSource{" +
                "id=" + id +
                ", rssUrl='" + rssUrl + '\'' +
                ", newsUrl='" + newsUrl + '\'' +
                ", category=" + category +
                ", cssSelector='" + cssSelector + '\'' +
                ", source=" + source +
                ", text='" + text + '\'' +
                '}';
    }
}
