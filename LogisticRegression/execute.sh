#!/bin/bash

echo "YES" | pio app data-delete TextClassification
cd /home/sedat/Desktop/code/prediction-trainer #change this with your path
mvn exec:java -Dexec.mainClass="com.sedooe.ExportDB"
cd /home/sedat/Desktop/code/LogisticRegression #change this with your path
pio import --appid 2 --input data/stopwords.json
pio import --appid 2 --input data/output.json
pio build
pio train
pio deploy&
gnome-terminal --tab -e "./getresult.sh"

